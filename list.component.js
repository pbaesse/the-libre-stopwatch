import React, {Component} from 'react';
import { ScrollView, FlatList, StyleSheet, Text } from 'react-native';

class ListComponent extends Component{

  render(){
    return(

      <FlatList
        style={styles.scroll}
        data={this.props.lap}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => <Text key={index+1} style={styles.item}> Lap {index.toString()}    -    {item.min}:{item.sec}:{item.msec} </Text>}
      />

    );
  }
}

const styles = StyleSheet.create({
    scroll: {
        maxHeight: "63%",
        backgroundColor: "#C89933",
        marginTop: "8%",
    },

    item: {
        padding: 10,
        paddingLeft: "20%",
        fontSize: 22,
        height: 47,
        color: "#5C415D",
        textAlign: "left",
        backgroundColor: "#C89933",
        marginBottom: 1
    },
});

export default ListComponent;
